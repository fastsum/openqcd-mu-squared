/*******************************************************************************
 *
 * File mu_squared.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__MU_SQUARED_H
#define OPENQCD__MU_SQUARED_H

#include "openqcd-propagator/c_headers/run_timer.h"
#include "openqcd/c_headers/su3.h"

/* MU_SQUARED_ONE_TO_ALL_C */
extern openqcd_run_timer__timing_info_t
openqcd_mu_squared__one_to_all_contributions(openqcd__complex_dble **out,
                                             int isrc, int *status);

/* MU_SQUARED_ALL_TO_ALL_C */
extern openqcd_run_timer__timing_info_t
openqcd_mu_squared__all_to_all_contributions(int nsrc,
                                             openqcd__complex_dble **out,
                                             int *status);

/* MU_SQUARED_BUFFER_C */
extern openqcd__spinor_dble **openqcd_mu_squared__mu_squared_buffer(void);

#if defined(OPENQCD_INTERNAL)

/* MU_SQUARED_ONE_TO_ALL_C */
#define one_to_all_contributions(...)                                          \
  openqcd_mu_squared__one_to_all_contributions(__VA_ARGS__)

/* MU_SQUARED_ALL_TO_ALL_C */
#define all_to_all_contributions(...)                                          \
  openqcd_mu_squared__all_to_all_contributions(__VA_ARGS__)

/* MU_SQUARED_BUFFER_C */
#define mu_squared_buffer(...)                                                 \
  openqcd_mu_squared__mu_squared_buffer(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__MU_SQUARED_H */
