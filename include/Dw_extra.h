/*******************************************************************************
 *
 * File Dw_extra.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__DW_EXTRA_H
#define OPENQCD__DW_EXTRA_H

#include "openqcd/c_headers/su3.h"

/* DW_EXTRA_C */
extern void openqcd_mu_squared__Dw_temporal_dble(int deriv,
                                                 openqcd__spinor_dble *s,
                                                 openqcd__spinor_dble *r);

#if defined(OPENQCD_INTERNAL)

/* DW_EXTRA_C */
#define Dw_temporal_dble(...) openqcd_mu_squared__Dw_temporal_dble(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__DW_EXTRA_H */
