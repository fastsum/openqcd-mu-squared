#!/bin/bash

format_cmd="clang-format-6.0"

dirs="include lib main"

find ${dirs} \
  \( -name "*.[hc]" -o -name "*.[hc]pp" \) \
  -exec ${format_cmd} -i -style=file {} \;
