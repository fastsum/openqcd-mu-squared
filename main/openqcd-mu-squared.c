/*******************************************************************************
 *
 * File openqcd-mu-squared.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Syntax: openqcd-mu-squared -i <input file> [-a]
 *
 * More detailed instructions in the project Readme.
 *
 *******************************************************************************/

#define OPENQCD_INTERNAL

#include "mu_squared.h"
#include "version.h"

#include "openqcd/c_headers/archive.h"
#include "openqcd/c_headers/dfl.h"
#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/lattice.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/random.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/stout_smearing.h"
#include "openqcd/c_headers/uflds.h"
#include "openqcd/c_headers/utils.h"
#include "openqcd/c_headers/version.h"

#include "openqcd-propagator/c_headers/propagator.h"
#include "openqcd-propagator/c_headers/sources.h"
#include "openqcd-propagator/c_headers/version.h"

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N0 (NPROC0 * L0)
#define N1 (NPROC1 * L1)
#define N2 (NPROC2 * L2)
#define N3 (NPROC3 * L3)

#define MAX(n, m)                                                              \
  if ((n) < (m))                                                               \
  (n) = (m)

static int my_rank, endian, append;
static int first, last, step;
static int num_kappa, num_noise_vecs, num_dfl_retries;
static int level, seed;
static int *rlxs_state = NULL, *rlxd_state = NULL;
static source_distribution_t source_dist;

static char log_dir[NAME_SIZE];
static char cnfg_dir[NAME_SIZE], out_dir[NAME_SIZE];
static char log_file[NAME_SIZE], log_save[NAME_SIZE], end_file[NAME_SIZE];
static char par_file[NAME_SIZE], par_save[NAME_SIZE];
static char rng_file[NAME_SIZE], rng_save[NAME_SIZE];
static char cnfg_file[NAME_SIZE], pfld_file[NAME_SIZE], nbase[NAME_SIZE];
static FILE *fin = NULL, *flog = NULL, *fend = NULL, *fpar = NULL;

/* Read the following sections from the infile:
 *  - Run name
 *  - Directories
 *  - Configurations
 *  - Random number generator
 */
static void read_dirs(void)
{
  if (my_rank == 0) {
    find_section("Run name");
    read_line("name", "%s", nbase);

    find_section("Directories");
    read_line("log_dir", "%s", log_dir);

    read_line("cnfg_dir", "%s", cnfg_dir);
    read_line("out_dir", "%s", out_dir);

    find_section("Configurations");
    read_line("first", "%d", &first);
    read_line("last", "%d", &last);
    read_line("step", "%d", &step);

    find_section("Random number generator");
    read_line("level", "%d", &level);
    read_line("seed", "%d", &seed);

    error_root((last < first) || (step < 1) || (((last - first) % step) != 0),
               1, "read_dirs [openqcd-mu-squared.c]",
               "Improper configuration range");
  }

  MPI_Bcast(nbase, NAME_SIZE, MPI_CHAR, 0, MPI_COMM_WORLD);

  MPI_Bcast(log_dir, NAME_SIZE, MPI_CHAR, 0, MPI_COMM_WORLD);
  MPI_Bcast(cnfg_dir, NAME_SIZE, MPI_CHAR, 0, MPI_COMM_WORLD);
  MPI_Bcast(out_dir, NAME_SIZE, MPI_CHAR, 0, MPI_COMM_WORLD);

  MPI_Bcast(&first, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&last, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&step, 1, MPI_INT, 0, MPI_COMM_WORLD);

  MPI_Bcast(&level, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&seed, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

/* Construct the filenames for where the output will be recorded such as
 * log_file, rng_file, etc.
 */
static void setup_files(void)
{
  error_root(name_size("%s/%sn%d", cnfg_dir, nbase, last) >= NAME_SIZE, 1,
             "setup_files [openqcd-mu-squared.c]", "cnfg_dir name is too long");

  check_dir_root(out_dir);
  error_root(name_size("%s/%sn%d.m1", out_dir, nbase, last) >= NAME_SIZE, 1,
             "setup_files [openqcd-mu-squared.c]", "out_dir name is too long");

  check_dir_root(log_dir);
  error_root(
      name_size("%s/%s.openqcd-mu-squared.log~", log_dir, nbase) >= NAME_SIZE,
      1, "setup_files [openqcd-mu-squared.c]", "log_dir name is too long");

  sprintf(log_file, "%s/%s.mu-squared.log", log_dir, nbase);
  sprintf(end_file, "%s/%s.mu-squared.end", log_dir, nbase);
  sprintf(par_file, "%s/%s.par", log_dir, nbase);
  sprintf(rng_file, "%s/%s.rng", log_dir, nbase);

  sprintf(log_save, "%s~", log_file);
  sprintf(par_save, "%s~", par_file);
  sprintf(rng_save, "%s~", rng_file);
}

#if !defined(STATIC_SIZES)
/* Parse "Lattice sizes" section of infile */
static void read_lattize_sizes(void)
{
  int local_lattice_sizes[4], mpi_layout[4], block_layout[4];

  if (my_rank == 0) {
    find_section("Lattice sizes");
    read_iprms("number_of_processes", 4, mpi_layout);
    read_iprms("local_lattice_sizes", 4, local_lattice_sizes);
    read_iprms("number_of_blocks", 4, block_layout);
  }

  mpc_bcast_i(mpi_layout, 4);
  mpc_bcast_i(local_lattice_sizes, 4);
  mpc_bcast_i(block_layout, 4);

  set_lattice_sizes(mpi_layout, local_lattice_sizes, block_layout);
}
#endif

/* Parse "Sources" section of infile */
static void read_sources(void)
{
  int nsrcs, isrc;
  char line[NAME_SIZE];

  if (my_rank == 0) {
    find_section("Sources");
    read_line("num_sources", "%d", &nsrcs);
    read_optional_line("random_dist", "%s", line, "RANDOM");

    if (strcmp(line, "RANDOM") == 0) {
      source_dist = RANDOM_DISTRIBUTION;
    } else if (strcmp(line, "MAX_DISTANCE") == 0) {
      source_dist = MAX_DISTANCE_DISTRIBUTION;
    } else {
      error_root(1, 1, "read_sources [openqcd-hadspec.c]",
                 "Unknown random source distribution %s", line);
    }
  }

  MPI_Bcast(&nsrcs, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&source_dist, 1, MPI_INT, 0, MPI_COMM_WORLD);

  for (isrc = 0; isrc < nsrcs; ++isrc) {
    read_source_parms(isrc);
  }
}

/* Parse "Dirac operator" section of infile */
static void read_dirac_parms(void)
{
  int smear;
  double *kappa, csw, mu;

  if (my_rank == 0) {
    find_section("Dirac operator");

    num_kappa = count_tokens("kappa");
    kappa = (double *)malloc(num_kappa * sizeof(*kappa));

    read_dprms("kappa", num_kappa, kappa);

    read_line("mu", "%lf", &mu);
    read_line("csw", "%lf", &csw);
    read_line("smear", "%d", &smear);
    smear = (smear != 0);
  }

  MPI_Bcast(&num_kappa, 1, MPI_INT, 0, MPI_COMM_WORLD);

  if (my_rank != 0) {
    kappa = (double *)malloc(num_kappa * sizeof(*kappa));
  }

  MPI_Bcast(kappa, num_kappa, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&mu, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&csw, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&smear, 1, MPI_INT, 0, MPI_COMM_WORLD);

  set_dirac_operator_parms(csw, mu, num_kappa, kappa, smear);

  if (append) {
    check_lat_parms(fpar);
  } else {
    write_lat_parms(fpar);
  }
}

/* Parse "Boundary conditions" section of infile */
static void read_bc_parms(void)
{
  int bc;
  double cF, cF_prime;
  double phi[2], phi_prime[2], theta[3];

  if (my_rank == 0) {
    find_section("Boundary conditions");
    read_line("type", "%d", &bc);

    phi[0] = 0.0;
    phi[1] = 0.0;
    phi_prime[0] = 0.0;
    phi_prime[1] = 0.0;
    cF = 1.0;
    cF_prime = 1.0;

    if (bc == 1) {
      read_dprms("phi", 2, phi);
    }

    if ((bc == 1) || (bc == 2)) {
      read_dprms("phi'", 2, phi_prime);
    }

    if (bc != 3) {
      read_line("cF", "%lf", &cF);
    }

    if (bc == 2) {
      read_line("cF'", "%lf", &cF_prime);
    }

    read_optional_dprms("theta", 3, theta);
  }

  MPI_Bcast(&bc, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(phi, 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(phi_prime, 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&cF, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&cF_prime, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(theta, 3, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  set_bc_parms(bc, 1.0, 1.0, cF, cF_prime, phi, phi_prime, theta);

  if (append) {
    check_bc_parms(fpar);
  } else {
    write_bc_parms(fpar);
  }
}

/* Parse "SAP" section of infile, only run if the solver needs SAP */
static void read_sap_parms(void)
{
  int bs[4];

  if (my_rank == 0) {
    find_section("SAP");
    read_line("bs", "%d %d %d %d", bs, bs + 1, bs + 2, bs + 3);
  }

  MPI_Bcast(bs, 4, MPI_INT, 0, MPI_COMM_WORLD);
  set_sap_parms(bs, 1, 4, 5);

  if (append) {
    check_sap_parms(fpar);
  } else {
    write_sap_parms(fpar);
  }
}

/* Parse "Deflation *" sections of infile, only run if the solver needs dfl */
static void read_dfl_parms(void)
{
  int bs[4], Ns;
  int ninv, nmr, ncy, nkv, nmx;
  double kappa, mu, res;

  if (my_rank == 0) {
    find_section("Deflation subspace");
    read_line("bs", "%d %d %d %d", bs, bs + 1, bs + 2, bs + 3);
    read_line("Ns", "%d", &Ns);
    read_optional_line("retries", "%d", &num_dfl_retries, 1);
  }

  MPI_Bcast(bs, 4, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&Ns, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&num_dfl_retries, 1, MPI_INT, 0, MPI_COMM_WORLD);
  set_dfl_parms(bs, Ns);

  if (my_rank == 0) {
    find_section("Deflation subspace generation");
    read_line("kappa", "%lf", &kappa);
    read_line("mu", "%lf", &mu);
    read_line("ninv", "%d", &ninv);
    read_line("nmr", "%d", &nmr);
    read_line("ncy", "%d", &ncy);
  }

  MPI_Bcast(&kappa, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&mu, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&ninv, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&nmr, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&ncy, 1, MPI_INT, 0, MPI_COMM_WORLD);
  set_dfl_gen_parms(kappa, mu, ninv, nmr, ncy);

  if (my_rank == 0) {
    find_section("Deflation projection");
    read_line("nkv", "%d", &nkv);
    read_line("nmx", "%d", &nmx);
    read_line("res", "%lf", &res);
  }

  MPI_Bcast(&nkv, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&nmx, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&res, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  set_dfl_pro_parms(nkv, nmx, res);

  if (append) {
    check_dfl_parms(fpar);
  } else {
    write_dfl_parms(fpar);
  }
}

/* Parse the "Solver 0" section of infile */
static void read_solver(void)
{
  solver_parms_t sp;

  read_solver_parms(0);
  sp = solver_parms(0);

  if (append) {
    check_solver_parms(fpar);
  } else {
    write_solver_parms(fpar);
  }

  if ((sp.solver == SAP_GCR) || (sp.solver == DFL_SAP_GCR)) {
    read_sap_parms();
  }

  if (sp.solver == DFL_SAP_GCR) {
    read_dfl_parms();
  }
}

/* Parse "Noise vectors" section of infile */
static void read_noise_vector(void)
{
  if (my_rank == 0) {
    find_section("Noise vectors");
    read_line("num", "%d", &num_noise_vecs);
  }

  MPI_Bcast(&num_noise_vecs, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

/* Parse "Anisotropy parameters" section of infile */
static void read_ani_parms(void)
{
  int has_ani;
  long section_pos;
  double nu, xi, cR, cT;

  if (my_rank == 0) {
    section_pos = find_optional_section("Anisotropy parameters");

    if (section_pos == No_Section_Found) {
      has_ani = 0;
    } else {
      has_ani = 1;
      read_line("nu", "%lf", &nu);
      read_line("xi", "%lf", &xi);
      read_line("cR", "%lf", &cR);
      read_line("cT", "%lf", &cT);
    }
  }

  mpc_bcast_i(&has_ani, 1);

  if (has_ani == 1) {
    mpc_bcast_d(&nu, 1);
    mpc_bcast_d(&xi, 1);
    mpc_bcast_d(&cR, 1);
    mpc_bcast_d(&cT, 1);

    set_ani_parms(0, nu, xi, cR, cT, 1.0, 1.0, 1.0, 1.0);
  } else {
    set_no_ani_parms();
  }

  if (append) {
    check_ani_parms(fpar);
  } else {
    write_ani_parms(fpar);
  }
}

/* Parse "Smearing parameters" section of infile */
static void read_smearing(void)
{
  long section_pos;
  int has_smearing = 0;
  int n_smear;
  double rho_t, rho_s;

  if (my_rank == 0) {
    section_pos = find_optional_section("Smearing parameters");

    if (section_pos == No_Section_Found) {
      has_smearing = 0;
    } else {
      has_smearing = 1;
      read_line("n_smear", "%d", &n_smear);
      read_line("rho_t", "%lf", &rho_t);
      read_line("rho_s", "%lf", &rho_s);
    }
  }

  mpc_bcast_i(&has_smearing, 1);

  if (has_smearing == 1) {
    mpc_bcast_i(&n_smear, 1);
    mpc_bcast_d(&rho_t, 1);
    mpc_bcast_d(&rho_s, 1);

    set_stout_smearing_parms(n_smear, rho_t, rho_s, 0, 1);
  } else {
    set_no_stout_smearing_parms();
  }

  if (append) {
    check_stout_smearing_parms(fpar);
  } else {
    write_stout_smearing_parms(fpar);
  }
}

/* Parse cmd line arguments, open the infile and parse its content */
static void read_infile(int argc, char *argv[])
{
  int ifile;

  if (my_rank == 0) {
    flog = freopen("STARTUP_ERROR", "w", stdout);

    ifile = find_opt(argc, argv, "-i");
    append = find_opt(argc, argv, "-a");
    endian = endianness();

    error_root((ifile == 0) || (ifile == (argc - 1)), 1,
               "read_infile [openqcd-mu-squared.c]",
               "Syntax: openqcd-mu-squared -i <input file> [-a]");

    error_root(endian == openqcd_utils__UNKNOWN_ENDIAN, 1,
               "read_infile [openqcd-mu-squared.c]",
               "Machine has unknown endianness");

    fin = freopen(argv[ifile + 1], "r", stdin);
    error_root(fin == NULL, 1, "read_infile [openqcd-mu-squared.c]",
               "Unable to open input file");
  }

  MPI_Bcast(&append, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&endian, 1, MPI_INT, 0, MPI_COMM_WORLD);

  read_dirs();
  setup_files();

  if (my_rank == 0) {
    if (append) {
      fpar = fopen(par_file, "rb");
    } else {
      fpar = fopen(par_file, "wb");
    }

    error_root(fpar == NULL, 1, "read_infile [openqcd-mu-squared.c]",
               "Unable to open parameter file");
  }

#if !defined(STATIC_SIZES)
  read_lattize_sizes();
#endif

  read_noise_vector();
  read_sources();
  read_ani_parms();
  read_smearing();
  read_dirac_parms();
  read_bc_parms();
  read_solver();

  if (my_rank == 0) {
    fclose(fin);
    fclose(fpar);

    if (append == 0) {
      copy_file(par_file, par_save);
    }
  }
}

/* Allocate memory for the one-all results */
static complex_dble **allocate_one_to_all_buffers(void)
{
  int idx;
  complex_dble **out;

  out = (complex_dble **)malloc(5 * sizeof(*out));

  error(out == NULL, 1, "allocate_one_to_all_buffers [openqcd-mu-squared.c]",
        "Unable to allocate one to all results pointer array");

  out[0] = (complex_dble *)amalloc(5 * 16 * L0 * sizeof(**out), ALIGN);

  error(out[0] == NULL, 1, "allocate_one_to_all_buffers [openqcd-mu-squared.c]",
        "Unable to allocate memory space for the all to all results");

  for (idx = 1; idx < 5; ++idx) {
    out[idx] = out[idx - 1] + 16 * L0;
  }

  return out;
}

/* Allocate memory for the all-all results */
static complex_dble **allocated_all_to_all_buffers(void)
{
  int itr;
  complex_dble **out;

  if (num_noise_vecs == 0) {
    return NULL;
  }

  out = (complex_dble **)malloc(3 * sizeof(*out));

  error(out == NULL, 1, "allocated_all_to_all_buffers [openqcd-mu-squared.c]",
        "Unable to allocate all to all results pointer array");

  out[0] = (complex_dble *)malloc(3 * num_noise_vecs * sizeof(**out));

  error(out[0] == NULL, 1,
        "allocated_all_to_all_buffers [openqcd-mu-squared.c]",
        "Unable to allocate memory space for the all to all results");

  for (itr = 1; itr < 3; ++itr) {
    out[itr] = out[itr - 1] + num_noise_vecs;
  }

  return out;
}

/* Initialise the RNG */
static void init_rng(int icnfg)
{
  int ic;

  if (append) {
    ic = import_ranlux(rng_file);
    error_root(ic != (icnfg - step), 1, "init_rng [openqcd-mu-squared.c]",
               "Configuration number mismatch (*.rng file)");
  } else {
    start_ranlux(level, seed);
  }
}

/* Check that we are not overwriting old datafiles */
static void check_files(void)
{
  if (my_rank == 0) {
    fin = fopen(log_file, "r");

    if (append) {
      error_root(fin == NULL, 1, "check_files [openqcd-mu-squared.c]",
                 "Continuation run:\n"
                 "Old logfile not present");

      fclose(fin);
    } else {
      error_root(fin != NULL, 1, "check_files [openqcd-mu-squared.c]",
                 "Attempt to overwrite old *.log or *.par file");
    }
  }
}

/* Parse the old logfile to see from which configuration we should continue the
 * analysis, only used for append runs (-a) */
static int find_continue_config(void)
{
  static char line[NAME_SIZE];
  int last_cfg, iread;
  FILE *foldlog;

  foldlog = fopen(log_file, "r");
  error_root(foldlog == NULL, 1, "find_continue_config [openqcd-mu-squared.c]",
             "Unable to open log file");

  last_cfg = 0;
  iread = 1;

  while (fgets(line, NAME_SIZE, foldlog) != NULL) {
    if (strstr(line, "Fully processed configuration no") != NULL) {
      iread &=
          (sscanf(line, "Fully processed configuration no %d", &last_cfg) == 1);
    }
  }

  fclose(foldlog);

  error_root(iread != 1, 1, "find_continue_config [openqcd-mu-squared.c]",
             "Incorrect read count");

  return last_cfg + step;
}

/* Print the header of the logfile, storing all relevant information about the
 * run so that it can be reconstructed */
static void print_info(void)
{
  int isap, idfl, n, imass;
  long ip;
  lat_parms_t lat;
  dirac_operator_parms_t dop;

  lat = lat_parms();
  dop = dirac_operator_parms();

  if (my_rank == 0) {
    ip = ftell(flog);
    fclose(flog);

    if (ip == 0L) {
      remove("STARTUP_ERROR");
    }

    if (append) {
      flog = freopen(log_file, "a", stdout);
    } else {
      flog = freopen(log_file, "w", stdout);
    }

    error_root(flog == NULL, 1, "print_info [openqcd-mu-squared.c]",
               "Unable to open log file");
    printf("\n");

    if (append) {
      printf("Continuation run, start from configuration nr. %d\n\n", first);
    } else {
      printf("Computation of mu^2 correlation functions\n");
      printf("-----------------------------------------\n\n");

      printf("Program build information:\n");
      printf("  major version: %s\n", openqcd_mu_squared__RELEASE);
      printf("  build date: %s\n", openqcd_mu_squared__build_date);
      printf("  git SHA: %s\n", openqcd_mu_squared__build_git_sha);
      printf("  user CFLAGS: %s\n\n", openqcd_mu_squared__build_user_cflags);

      printf("Based on the following openQCD library:\n");
      printf("  major version: %s\n", openQCD_RELEASE);
      printf("  build date: %s\n", build_date);
      printf("  git SHA: %s\n", build_git_sha);
      printf("  user CFLAGS: %s\n\n", build_user_cflags);

      printf("Based on the following openQCD-propagator library:\n");
      printf("  major version: %s\n", openqcd_propagator_RELEASE);
      printf("  build date: %s\n", openqcd_propagator__build_date);
      printf("  git SHA: %s\n", openqcd_propagator__build_git_sha);
      printf("  user CFLAGS: %s\n\n", openqcd_propagator__build_user_cflags);
    }

    if (endian == openqcd_utils__LITTLE_ENDIAN) {
      printf("The machine is little endian\n");
    } else {
      printf("The machine is big endian\n");
    }

    printf("Configurations are read in exported file format\n\n");

    if (append == 0) {
      printf("%dx%dx%dx%d lattice, ", N0, N1, N2, N3);
      printf("%dx%dx%dx%d local lattice\n", L0, L1, L2, L3);
      printf("%dx%dx%dx%d process grid, ", NPROC0, NPROC1, NPROC2, NPROC3);
      printf("%dx%dx%dx%d process block size\n", NPROC0_BLK, NPROC1_BLK,
             NPROC2_BLK, NPROC3_BLK);
    }

    printf("Random number generator:\n");

    if (append == 0) {
      printf("level = %d, seed = %d\n\n", level, seed);
    } else {
      printf("State of ranlxs and ranlxd reset to the\n");
      printf("last exported state\n\n");
    }

    if (append == 0) {
      printf("Dirac operator:\n");

      printf("kappa = ");

      for (imass = 0; imass < num_kappa; ++imass) {
        n = fdigits(lat.kappa[imass]);
        printf("%.*f ", IMAX(n, 6), lat.kappa[imass]);
      }

      printf("\n");

      n = fdigits(dop.mu);
      printf("mu = %.*f\n", IMAX(n, 1), dop.mu);
      n = fdigits(lat.csw);
      printf("csw = %.*f\n", IMAX(n, 1), lat.csw);
      printf("smear = %s\n\n", (dop.smear) ? "true" : "false");

      print_bc_parms(2);
      print_ani_parms();
      print_stout_smearing_parms();

      print_source_parms();

      print_solver_parms(&isap, &idfl);

      if (isap) {
        print_sap_parms(0);
      }

      if (idfl) {
        print_dfl_parms(0);
      }
    }

    printf("Configurations no %d -> %d in steps of %d\n\n", first, last, step);
    fflush(flog);
  }
}

/* Log information after a single step (one-all / all-all) has been computed */
static void print_step_computation_info(int const status[], char const *descr,
                                        int mass_no, timing_info_t calc_time)
{
  static dfl_parms_t dfl;
  static lat_parms_t lat;
  dfl = dfl_parms();
  lat = lat_parms();

  if (my_rank == 0) {
    printf("Computation of %s for mass no %d (%f) complete \n", descr, mass_no,
           lat.kappa[mass_no]);

    if (dfl.Ns) {
      printf("status = %d,%d", status[0], status[1]);

      if (status[2]) {
        printf(" (no of subspace regenerations = %d)\n", status[2]);
      } else {
        printf("\n");
      }
    } else {
      printf("status = %d\n", status[0]);
    }

    printf("Total inversions: %d in %.2e seconds\n", calc_time.nops,
           calc_time.total_time);
    printf("%.2e sec per solution (average %.2e sec)\n\n", calc_time.latest,
           avg_time(&calc_time));

    fflush(flog);
    copy_file(log_file, log_save);
  }
}

/* Lof information after a full config has been processed */
static void print_full_step_computation_info(int config_no,
                                             timing_info_t total_time)
{
  if (my_rank == 0) {
    printf("Fully processed configuration no %d in %.2e sec ", config_no,
           total_time.latest);
    printf("(average = %.2e sec)\n\n\n", avg_time(&total_time));

    fflush(flog);
    copy_file(log_file, log_save);
  }
}

/* Communicate the one-all results, ordering them in time based on the origin of
 * the source and store it in a file */
static void store_one_to_all_results(complex_dble **out, int isrc,
                                     char const *filename)
{
  FILE *fop;
  complex_dble **savebuf, **rbuf;
  int it, ig, modit, full_it, itr, tag, mpi_next;
  MPI_Status stat;
  source_parms_t src;

  tag = mpi_tag();
  src = source_parms(isrc);

  if (my_rank == 0) {
    savebuf = (complex_dble **)malloc(5 * sizeof(*savebuf));

    error_root(savebuf == NULL, 1,
               "store_one_to_all_results [openqcd-mu-squared.c]",
               "Unable to allocate output buffer array");

    /* Need to reshuffle the output depending on the position of the source,
     * therefore we store the entire time-extent in a temporary matrix which we
     * fill in such a way that the time-slice the source is on is t=0 */
    savebuf[0] =
        (complex_dble *)amalloc(5 * 16 * N0 * sizeof(**savebuf), ALIGN);

    error_root(savebuf[0] == NULL, 1,
               "store_one_to_all_results [openqcd-mu-squared.c]",
               "Unable to allocate output buffer memory");

    for (itr = 1; itr < 5; ++itr) {
      savebuf[itr] = savebuf[itr - 1] + 16 * N0;
    }

    /* Fill the local contribution */
    for (it = 0, full_it = 0; it < L0; ++it, ++full_it) {
      modit = (N0 + full_it - src.pos[0]) % N0;
      for (ig = 0; ig < 16; ++ig) {
        for (itr = 0; itr < 5; ++itr) {
          savebuf[itr][ig + 16 * modit] = out[itr][ig + 16 * it];
        }
      }
    }

    /* If there are more processes in t-dir we need to fetch these */
    if (NPROC0 > 1) {

      rbuf = (complex_dble **)malloc(5 * sizeof(*rbuf));

      error_root(rbuf == NULL, 1,
                 "store_one_to_all_results [openqcd-mu-squared.c]",
                 "Unable to allocate communication buffer array");

      rbuf[0] = (complex_dble *)amalloc(5 * 16 * L0 * sizeof(**rbuf), ALIGN);

      error_root(rbuf[0] == NULL, 1,
                 "store_one_to_all_results [openqcd-mu-squared.c]",
                 "Unable to allocate communication buffer memory");

      for (itr = 1; itr < 5; ++itr) {
        rbuf[itr] = rbuf[itr - 1] + 16 * L0;
      }

      mpi_next = npr[1];

      while (mpi_next != 0) {

        /* Fetch the data */
        MPI_Recv(*rbuf, 10 * 16 * L0, MPI_DOUBLE, mpi_next, tag, MPI_COMM_WORLD,
                 &stat);

        /* Fetch the processor id of the next neighbour */
        MPI_Recv(&mpi_next, 1, MPI_INT, mpi_next, tag, MPI_COMM_WORLD, &stat);

        /* Place data appropriately in the savebuf matrix */
        for (it = 0; it < L0; ++it, ++full_it) {
          modit = (N0 + full_it - src.pos[0]) % N0;
          for (ig = 0; ig < 16; ++ig) {
            for (itr = 0; itr < 5; ++itr) {
              savebuf[itr][ig + 16 * modit] = rbuf[itr][ig + 16 * it];
            }
          }
        }
      }

      /* Free the MPI communication buffer memory */
      afree(rbuf[0]);
      free(rbuf);
    }

    /* Open the output file for writing */
    fop = fopen(filename, "w");

    /* Write the reordered data to file */
    for (it = 0; it < 16 * N0; ++it) {
      fprintf(fop, "%d %d ", it / 16, it % 16);
      for (itr = 0; itr < 5; ++itr) {
        fprintf(fop, "%.12e %.12e ", savebuf[itr][it].re, savebuf[itr][it].im);
      }
      fprintf(fop, "\n");
    }

    fclose(fop);

    /* Free the reordering matrix buffer memory */
    afree(savebuf[0]);
    free(savebuf);
  } else if ((cpr[1] + cpr[2] + cpr[3]) == 0) {
    /* If you are a {nx, ny, nz} = 0 node, you need to send your data to
     * processor 0 */
    MPI_Send(*out, 10 * 16 * L0, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
    MPI_Send(npr + 1, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
  }
}

/* Store the one-all results in a file */
static void store_all_to_all_results(complex_dble **out, char const *filename)
{
  int isrc, itr;
  FILE *fop;

  if (my_rank == 0) {
    fop = fopen(filename, "w");

    for (isrc = 0; isrc < num_noise_vecs; ++isrc) {
      for (itr = 0; itr < 3; ++itr) {
        fprintf(fop, "%.12e %.12e ", out[itr][isrc].re, out[itr][isrc].im);
      }
      fprintf(fop, "\n");
    }

    fclose(fop);
  }
}

/* Compute the required workspace size from deflation operations */
static void dfl_wsize(int *nws, int *nwv, int *nwvd)
{
  dfl_parms_t dp;
  dfl_pro_parms_t dpp;

  dp = dfl_parms();
  dpp = dfl_pro_parms();

  MAX(*nws, dp.Ns + 2);
  MAX(*nwv, 2 * dpp.nkv + 2);
  MAX(*nwvd, 4);
}

/* Calculate the toatal required workspace size */
static void wsize(int *nws, int *nwsd, int *nwv, int *nwvd)
{
  int nsd;
  solver_parms_t sp;

  (*nws) = 0;
  (*nwsd) = 0;
  (*nwv) = 0;
  (*nwvd) = 0;

  sp = solver_parms(0);
  nsd = 3;

  if (sp.solver == CGNE) {
    MAX(*nws, 5);
    MAX(*nwsd, nsd + 3);
  } else if (sp.solver == SAP_GCR) {
    MAX(*nws, 2 * sp.nkv + 1);
    MAX(*nwsd, nsd + 2);
  } else if (sp.solver == DFL_SAP_GCR) {
    MAX(*nws, 2 * sp.nkv + 2);
    MAX(*nwsd, nsd + 4);
    dfl_wsize(nws, nwv, nwvd);
  } else {
    error_root(1, 1, "wsize [openqcd-mu-squared.c]",
               "Unknown or unsupported solver");
  }
}

/* Check if an endflag file exists, if so, stop the simulation */
static void check_endflag(int *iend)
{
  if (my_rank == 0) {
    fend = fopen(end_file, "r");

    if (fend != NULL) {
      fclose(fend);
      remove(end_file);
      (*iend) = 1;
      printf("End flag set, run stopped\n\n");
    } else {
      (*iend) = 0;
    }
  }

  MPI_Bcast(iend, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

int main(int argc, char *argv[])
{
  int nc, iend, status[3], i;
  int nws, nwsd, nwv, nwvd, imass, isrc, ndfl;
  double start_time;
  complex_dble **one_to_all_out, **all_to_all_out;
  timing_info_t inversion_timing = {0.0, 0.0, 0};
  timing_info_t computation_timing = {0.0, 0.0, 0};
  dfl_parms_t dfl;
  dirac_operator_parms_t dop;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  read_infile(argc, argv);
  check_files();

  if (append) {
    first = find_continue_config();
  }

  print_info();
  dfl = dfl_parms();
  dop = dirac_operator_parms();

  init_rng(first);
  geometry();

  wsize(&nws, &nwsd, &nwv, &nwvd);
  alloc_ws(nws);
  alloc_wsd(nwsd);
  alloc_wv(nwv);
  alloc_wvd(nwvd);

  one_to_all_out = allocate_one_to_all_buffers();
  all_to_all_out = allocated_all_to_all_buffers();

  iend = 0;

  for (nc = first; (iend == 0) && (nc <= last); nc += step) {
    start_time = start_timer();

    if (my_rank == 0) {
      printf("Configuration no %d\n", nc);
    }

    /* Read gauge configuration */
    sprintf(cnfg_file, "%s/%sn%d", cnfg_dir, nbase, nc);
    message("Opening file: %s\n", cnfg_file);
    import_cnfg(cnfg_file);

    set_ud_phase();

    /* Generate the deflation subspace */
    if (dfl.Ns) {
      if (dop.smear) {
        smear_fields();
      }

      ndfl = 0;
      status[0] = -1;
      while (status[0] < 0 && (ndfl < num_dfl_retries)) {
        for (i = 0; i < 3; ++i) {
          status[i] = 0;
        }

        dfl_modes(status);
        ndfl += 1;
      }

      error_root(
          status[0] < 0, 1, "main [openqcd-propagator.c]",
          "Deflation subspace generation failed (status = %d, retries = %d)",
          status[0], ndfl);

      if (my_rank == 0) {
        printf("Deflation subspace generation: status = %d, retries = %d\n\n",
               status[0], ndfl);
      }

      if (dop.smear) {
        unsmear_fields();
      }
    }

    distribute_random_sources(source_dist, 1);

    /* Loop over the quark masses to compute */
    for (imass = 0; imass < num_kappa; ++imass) {
      set_sw_parms(sea_quark_mass(imass));

      /* Compute the one-all constributions */
      isrc = next_source_id(0);
      while (isrc >= 0) {
        if (my_rank == 0) {
          printf("Computing source %d\n", isrc);
        }

        inversion_timing =
            one_to_all_contributions(one_to_all_out, isrc, status);

        print_step_computation_info(status, "one to all contributions", imass,
                                    inversion_timing);

        sprintf(pfld_file, "%s/%sn%d.one-all.s%d.m%d", out_dir, nbase, nc, isrc,
                imass);
        store_one_to_all_results(one_to_all_out, isrc, pfld_file);

        isrc = next_source_id(isrc + 1);
      }

      /* Skip all-all if there are no random sources */
      if (num_noise_vecs == 0) {
        continue;
      }

      /* Compute the all-all contributions */
      inversion_timing =
          all_to_all_contributions(num_noise_vecs, all_to_all_out, status);

      print_step_computation_info(status, "all to all contributions", imass,
                                  inversion_timing);

      sprintf(pfld_file, "%s/%sn%d.all-all.m%d", out_dir, nbase, nc, imass);
      store_all_to_all_results(all_to_all_out, pfld_file);
    }

    stop_timer(&computation_timing, start_time);
    export_ranlux(nc, rng_file);

    /* Print success information */
    print_full_step_computation_info(nc, computation_timing);

    check_endflag(&iend);

    if (my_rank == 0) {
      fflush(flog);
      copy_file(log_file, log_save);
      copy_file(rng_file, rng_save);
    }
  }

  if (my_rank == 0) {
    fflush(flog);
    copy_file(log_file, log_save);
    fclose(flog);
  }

  MPI_Finalize();
  exit(0);
}
