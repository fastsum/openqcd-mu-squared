/*******************************************************************************
 *
 * File mu_squared_buffer.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   spinor_dble **mu_squared_buffer(void)
 *     Returns a buffer of spinor fields necessary to carry out the mu-squared
 *     computation. If the memory for this hasn't been allocated, it will be the
 *     first time this functions is called.
 *
 *     The buffer contains 24 spinor fields and 1 communication buffer. The
 *     order in which computations are carried out has to be handled with care
 *     to make sure no data is overwritten when operators that make use of
 *     communication is called.
 *
 *******************************************************************************/

#define MU_SQUARED_BUFFER_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "mu_squared.h"

#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

static spinor_dble **pbuffer = NULL;

/* Allocate the memory for the buffer */
static void allocate_mu_squared_buffer(void)
{
  int i;

  error(iup[0][0] == 0, 1, "allocate_mu_squared_buffer [mu_squared_buffer.c]",
        "Geometry arrays are not set");

  pbuffer = (spinor_dble **)malloc(24 * sizeof(*pbuffer));

  error(pbuffer == NULL, 1, "allocate_mu_squared_buffer [mu_squared_buffer.c]",
        "Unable to allocate buffer field pointer array");

  /* We allocate 24 spinor fields as well as 1 set of spin boundaries, this will
   * make it so that the i'th propagator component spinor field will use the
   * (i+1)'th propagator as a buffer, but as they are computed sequentially,
   * this doesn't matter. Only the final propagator has its own boundary buffer.
   */
  pbuffer[0] = (spinor_dble *)amalloc(
      (24 * VOLUME + BNDRY / 2) * sizeof(**pbuffer), ALIGN);

  error(pbuffer[0] == NULL, 1, "allocate_propagator_field [propagator_field.c]",
        "Unable to allocate memory space for the propagator fields");

  for (i = 1; i < 24; i++) {
    pbuffer[i] = pbuffer[i - 1] + VOLUME;
  }
}

spinor_dble **mu_squared_buffer(void)
{
  if (pbuffer == NULL) {
    allocate_mu_squared_buffer();
  }

  return pbuffer;
}
