/*******************************************************************************
 *
 * File mu_squared_disconnected.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   timing_info_t all_to_all_contributions(int nsrc, complex_dble **out,
 *                                          int *status)
 *     Compute the all-all contributions of the mu-squared computations. These
 *     are estimated using the noise-vector method, the number of noise vectors
 *     is given by nsrc.
 *
 *     Status of the necessary inversions are stored in status, program returns
 *     information on timing. Results from the computation is stored in "out"
 *     which is expected to be a 3 x nsrcs array of complex numbers.
 *
 *******************************************************************************/

#define MU_SQUARED_ALL_TO_ALL_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "Dw_extra.h"
#include "mu_squared.h"

#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/stout_smearing.h"
#include "openqcd/c_headers/utils.h"

#include "openqcd-propagator/c_headers/propagator.h"
#include "openqcd-propagator/c_headers/sources.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>

timing_info_t all_to_all_contributions(int nsrc, complex_dble **out,
                                       int *status)
{
  int my_rank, l, isrc, stat[3];
  spinor_dble *eta, *psi, *xi, **wsd;
  double start_time;
  timing_info_t tinfo = {0.0, 0.0, 0};
  dirac_operator_parms_t dop;

  /* Exit if there are no sources */
  if (nsrc == 0)
    return tinfo;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  wsd = reserve_wsd(3);
  eta = wsd[0];
  psi = wsd[1];
  xi = wsd[2];

  dop = dirac_operator_parms();

  /* Reset the inversion status */
  for (l = 0; l < 3; l++) {
    status[l] = 0;
    stat[l] = 0;
  }

  /* Smear the gauge field if necessary */
  if (dop.smear) {
    smear_fields();
  }

  /* Compute the matrices (D^-1) and (D^-1 dotD D^-1) */
  for (isrc = 0; isrc < nsrc; ++isrc) {

    start_time = start_timer();

    random_sd(VOLUME, eta, 1.0);

    solve_dirac(eta, psi, 0, stat);
    Dw_temporal_dble(1, psi, xi);
    out[0][isrc] = spinor_prod_dble(VOLUME, 1, eta, xi);

    Dw_temporal_dble(2, psi, xi);
    out[1][isrc] = spinor_prod_dble(VOLUME, 1, eta, xi);

    stop_timer(&tinfo, start_time);

    start_time = start_timer();

    Dw_temporal_dble(1, psi, xi);
    solve_dirac(xi, psi, 0, stat);
    Dw_temporal_dble(1, psi, xi);
    out[2][isrc] = spinor_prod_dble(VOLUME, 1, eta, xi);

    stop_timer(&tinfo, start_time);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }
  }

  /* Unsmear the gauge field */
  if (dop.smear) {
    unsmear_fields();
  }

  for (l = 0; l < 2; l++) {
    status[l] = (status[l] + (nsrc)) / (2 * nsrc);
  }

  release_wsd();
  return tinfo;
}
