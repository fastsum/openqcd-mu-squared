/*******************************************************************************
 *
 * File Dw_extra.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void Dw_temporal_dble(int deriv, spinor_dble *s, spinor_dble *r)
 *     Applies the temporal hopping Dirac operator to spinor field s and stores
 *     the result in r. Since the operator is intended to be a definition of the
 *     n'th derivative of the Wilson-Dirac operator with respect to the chemical
 *     potential a "deriv" parameter is supplied to tell which order of
 *     derivative to use. The effect of this parameter is that the negative
 *     direction hopping term will have a prefactor (-1)^n, so negative for odd
 *     orders and positive for even orders.
 *
 * Notes:
 *
 *   The routine copies more information from its boundary than strictly
 *   necessary, but this should be a small part of the overall computation of
 *   the program.
 *
 *   Similar to other implementations of Dirac-like operators this one is also
 *   not const-correct due to the fact that the halo is treated as mutable data
 *   also for the input field.
 *
 *******************************************************************************/

#define DW_EXTRA_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "Dw_extra.h"

#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/lattice.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/uflds.h"

#ifdef __cplusplus
}
#endif

typedef union
{
  spinor_dble s;
  weyl_dble w[2];
} spin_t;

static int odd_derivative = 0;
static double coe, ceo;
static spin_t rs ALIGNED32;

#define _vector_mul_assign(r, c)                                               \
  (r).c1.re *= (c);                                                            \
  (r).c1.im *= (c);                                                            \
  (r).c2.re *= (c);                                                            \
  (r).c2.im *= (c);                                                            \
  (r).c3.re *= (c);                                                            \
  (r).c3.im *= (c)

/* Similar to how the Dirac operator is implemented in openQCD we compute the
 * nearest-neighbour contributions in two stages. This is the first one collects
 * contributions from neighbours into the currently active point (which is odd).
 */
static void doe_temporal_dble(int *piup, int *pidn, su3_dble *u,
                              spinor_dble *pk)
{
  spinor_dble *sp, *sm;
  su3_vector_dble psi, chi;

  /****************************** direction +0 ********************************/

  sp = pk + (*(piup++));

  _vector_add(psi, (*sp).c1, (*sp).c3);
  _su3_multiply(rs.s.c1, *u, psi);
  rs.s.c3 = rs.s.c1;

  _vector_add(psi, (*sp).c2, (*sp).c4);
  _su3_multiply(rs.s.c2, *u, psi);
  rs.s.c4 = rs.s.c2;

  /****************************** direction -0 ********************************/

  sm = pk + (*(pidn++));
  u += 1;

  _vector_sub(psi, (*sm).c1, (*sm).c3);
  _su3_inverse_multiply(chi, *u, psi);

  if (odd_derivative == 1) {
    _vector_sub_assign(rs.s.c1, chi);
    _vector_add_assign(rs.s.c3, chi);
  } else {
    _vector_add_assign(rs.s.c1, chi);
    _vector_sub_assign(rs.s.c3, chi);
  }

  _vector_sub(psi, (*sm).c2, (*sm).c4);
  _su3_inverse_multiply(chi, *u, psi);

  if (odd_derivative == 1) {
    _vector_sub_assign(rs.s.c2, chi);
    _vector_add_assign(rs.s.c4, chi);

  } else {
    _vector_add_assign(rs.s.c2, chi);
    _vector_sub_assign(rs.s.c4, chi);
  }

  _vector_mul_assign(rs.s.c1, coe);
  _vector_mul_assign(rs.s.c2, coe);
  _vector_mul_assign(rs.s.c3, coe);
  _vector_mul_assign(rs.s.c4, coe);
}

/* This is the second stage of the hopping term, this time it distributes the
 * currently active odd point to its nearest neighbours */
static void deo_temporal_dble(int *piup, int *pidn, su3_dble *u,
                              spinor_dble *pl)
{
  spinor_dble *sp, *sm;
  su3_vector_dble psi, chi;

  _vector_mul_assign(rs.s.c1, ceo);
  _vector_mul_assign(rs.s.c2, ceo);
  _vector_mul_assign(rs.s.c3, ceo);
  _vector_mul_assign(rs.s.c4, ceo);

  /****************************** direction +0 ********************************/

  sp = pl + (*(piup++));

  _vector_sub(psi, rs.s.c1, rs.s.c3);
  _su3_inverse_multiply(chi, *u, psi);

  if (odd_derivative == 1) {
    _vector_sub_assign((*sp).c1, chi);
    _vector_add_assign((*sp).c3, chi);
  } else {
    _vector_add_assign((*sp).c1, chi);
    _vector_sub_assign((*sp).c3, chi);
  }

  _vector_sub(psi, rs.s.c2, rs.s.c4);
  _su3_inverse_multiply(chi, *u, psi);

  if (odd_derivative == 1) {
    _vector_sub_assign((*sp).c2, chi);
    _vector_add_assign((*sp).c4, chi);
  } else {
    _vector_add_assign((*sp).c2, chi);
    _vector_sub_assign((*sp).c4, chi);
  }

  /****************************** direction -0 ********************************/

  sm = pl + (*(pidn++));
  u += 1;

  _vector_add(psi, rs.s.c1, rs.s.c3);
  _su3_multiply(chi, *u, psi);
  _vector_add_assign((*sm).c1, chi);
  _vector_add_assign((*sm).c3, chi);

  _vector_add(psi, rs.s.c2, rs.s.c4);
  _su3_multiply(chi, *u, psi);
  _vector_add_assign((*sm).c2, chi);
  _vector_add_assign((*sm).c4, chi);
}

void Dw_temporal_dble(int deriv, spinor_dble *s, spinor_dble *r)
{
  int *piup, *pidn;
  su3_dble *u, *um;
  spin_t *so, *ro;
  ani_params_t ani;

  cpsd_int_bnd(0x1, s);
  set_sd2zero(NSPIN, r);

  ani = ani_parms();

  coe = -0.5 / ani.ut_fermion;
  ceo = -0.5 / ani.ut_fermion;
  odd_derivative = (deriv % 2);

  piup = iup[VOLUME / 2];
  pidn = idn[VOLUME / 2];

  so = (spin_t *)(s + (VOLUME / 2));
  ro = (spin_t *)(r + (VOLUME / 2));
  u = udfld();
  um = u + 4 * VOLUME;

  for (; u < um; u += 8, piup += 4, pidn += 4, ++so, ++ro) {
    doe_temporal_dble(piup, pidn, u, s);

    _vector_add_assign((*ro).s.c1, rs.s.c1);
    _vector_add_assign((*ro).s.c2, rs.s.c2);
    _vector_add_assign((*ro).s.c3, rs.s.c3);
    _vector_add_assign((*ro).s.c4, rs.s.c4);
    rs = (*so);

    deo_temporal_dble(piup, pidn, u, r);
  }

  cpsd_ext_bnd(0x1, r);
}
