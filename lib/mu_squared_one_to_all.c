/*******************************************************************************
 *
 * File mu_squared.c
 *
 * Author (2018): Jonas Rylund Glesaaen
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 *
 * The externally accessible functions are
 *
 *   timing_info_t one_to_all_contributions(complex_dble **out, int isrc,
 *                                          int *status)
 *    Compute the one-all contributions to the mu-squared corrections to meson
 *    correlators. It used the source parameters as specified by the source in
 *    isrc.
 *
 *    Status of the inversions is stored in status and the related inversion
 *    timings are returned. The result from the computation is stored in out
 *    which is a 5 x (16 * L0) array of complex numbers.
 *
 *******************************************************************************/

#define MU_SQUARED_ONE_TO_ALL_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "Dw_extra.h"
#include "gamma_prod.h"
#include "mu_squared.h"

#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/stout_smearing.h"
#include "openqcd/c_headers/utils.h"

#include "openqcd-propagator/c_headers/propagator.h"
#include "openqcd-propagator/c_headers/sources.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>

MPI_Comm *spatial_mpi_communicator = NULL;

typedef union
{
  spinor_dble s;
  complex_dble c[12];
} spin_t;

/* Make MPI communication groups so that we can do sums over space but not time
 * accross processes */
static void allocate_spatial_MPI_group(int my_rank)
{
  spatial_mpi_communicator =
      (MPI_Comm *)malloc(sizeof(*spatial_mpi_communicator));
  MPI_Comm_split(MPI_COMM_WORLD, cpr[0], my_rank, spatial_mpi_communicator);
}

/* Swap x and y and do the complex conjugate */
static void swap_conjugate(complex_dble *x, complex_dble *y)
{
  complex_dble tmp = *y;

  (*y).re = (*x).re;
  (*y).im = -(*x).im;

  (*x).re = tmp.re;
  (*x).im = -tmp.im;
}

/* Replace s by s^{dagger} for a 12x12 spin-colour matrix */
static void prop_dagger(spin_t *s)
{
  int sci, scj;

  for (sci = 0; sci < 12; ++sci) {
    for (scj = sci; scj < 12; ++scj) {
      swap_conjugate(&(s[sci].c[scj]), &(s[scj].c[sci]));
    }
  }
}

/* For two one-to-all propagators A and B compute the term:
 *   tr[g5 G^dagger A G g5 B] (trace over spin and colour)
 * for all 16 gamma matrices G and store the result in out */
static void full_trace_contribution(complex_dble *out, spinor_dble **A,
                                    spinor_dble **B)
{
  int ix, it, ig, sci, scj;
  complex_dble *sum_buffer;
  spin_t bufferA[12] ALIGNED32;
  spin_t bufferB[12] ALIGNED32;

  sum_buffer = (complex_dble *)amalloc(16 * L0 * sizeof(*sum_buffer), ALIGN);

  for (it = 0; it < 16 * L0; ++it) {
    sum_buffer[it].re = 0.0;
    sum_buffer[it].im = 0.0;
  }

  /* Loop over the lattice volume */
  for (ix = 0; ix < VOLUME; ++ix) {

    it = ix / (L1 * L2 * L3);

    /* Loop over the 16 gamma matrices */
    for (ig = 0; ig < 16; ++ig) {

      /* Get buffers and left multiply A by g5G^dagger */
      for (sci = 0; sci < 12; ++sci) {
        bufferA[sci].s = A[sci][ipt[ix]];
        g5_gamma_dag_prod(&(bufferA[sci].s), ig);

        bufferB[sci].s = B[sci][ipt[ix]];
      }

      /* Conjugate transpose the B matrix */
      prop_dagger(bufferB);

      /* Left multiply B with G*g5 */
      for (sci = 0; sci < 12; ++sci) {
        gamma_g5_prod(&(bufferB[sci].s), ig);
      }

      /* Compute the spin/colour-trace */
      for (sci = 0; sci < 12; ++sci) {
        for (scj = 0; scj < 12; ++scj) {
          sum_buffer[16 * it + ig].re +=
              bufferA[sci].c[scj].re * bufferB[scj].c[sci].re -
              bufferA[sci].c[scj].im * bufferB[scj].c[sci].im;
          sum_buffer[16 * it + ig].im +=
              bufferA[sci].c[scj].im * bufferB[scj].c[sci].re +
              bufferA[sci].c[scj].re * bufferB[scj].c[sci].im;
        }
      }
    }
  }

  /* Sum over the spatial subprocesses */
  MPI_Allreduce(sum_buffer, out, 2 * 16 * L0, MPI_DOUBLE, MPI_SUM,
                *spatial_mpi_communicator);

  /* Free memory of sum_buffer */
  afree(sum_buffer);
}

timing_info_t one_to_all_contributions(complex_dble **out, int isrc,
                                       int *status)
{
  int my_rank, sci, l, stat[3];
  spinor_dble *eta, *psi, **wsd, **gbuffer, *bndry_buffer;
  double start_time;
  timing_info_t tinfo = {0.0, 0.0, 0};
  dirac_operator_parms_t dop;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (spatial_mpi_communicator == NULL) {
    allocate_spatial_MPI_group(my_rank);
  }

  bndry_buffer =
      (spinor_dble *)amalloc((BNDRY / 2) * sizeof(*bndry_buffer), ALIGN);

  gbuffer = mu_squared_buffer();
  wsd = reserve_wsd(2);
  eta = wsd[0];
  psi = wsd[1];

  dop = dirac_operator_parms();

  /* Reset the inversion status */
  for (l = 0; l < 3; l++) {
    status[l] = 0;
    stat[l] = 0;
  }

  /* Smear the gauge field if necessary */
  if (dop.smear) {
    smear_fields();
  }

  /* Compute the matrices (D^-1) and (D^-1 dotD D^-1) */
  for (sci = 0; sci < 12; ++sci) {
    start_time = start_timer();

    generate_source(isrc, sci, eta);
    solve_dirac(eta, gbuffer[sci], 0, stat);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    stop_timer(&tinfo, start_time);
  }

  /* Need to do it in two loops so that the shared buffer doesn't overwrite
   * anything */
  for (sci = 0; sci < 12; ++sci) {

    start_time = start_timer();

    assign_sd2sd(VOLUME, gbuffer[sci], psi);
    Dw_temporal_dble(1, psi, eta);
    solve_dirac(eta, gbuffer[12 + sci], 0, stat);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    stop_timer(&tinfo, start_time);

    status[2] += (stat[2] != 0);
  }

  /* Traces 3, 4 & 5 can be computed with these two sets */
  full_trace_contribution(out[2], gbuffer + 12, gbuffer + 12);
  full_trace_contribution(out[3], gbuffer + 12, gbuffer);
  full_trace_contribution(out[4], gbuffer, gbuffer);

  /* Compute the matrices (D^-1) and (D^-1 (dotD D^-1)^2) */
  /* Need to do a bit of boundary link management */
  for (sci = 0; sci < 12; ++sci) {
    start_time = start_timer();

    /* Copy a boundary buffer size chunk of the next propagator element */
    if (BNDRY > 0 && sci < 11) {
      assign_sd2sd(BNDRY / 2, gbuffer[12 + sci + 1], bndry_buffer);
    }

    assign_sd2sd(VOLUME, gbuffer[12 + sci], psi);
    Dw_temporal_dble(1, psi, eta);
    solve_dirac(eta, gbuffer[12 + sci], 0, stat);

    /* Reinstate the previously stored boundary elements */
    if (BNDRY > 0 && sci < 11) {
      assign_sd2sd(BNDRY / 2, bndry_buffer, gbuffer[12 + sci + 1]);
    }

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    stop_timer(&tinfo, start_time);
  }

  /* Trace 1 can be computed with these two sets */
  full_trace_contribution(out[0], gbuffer + 12, gbuffer);

  /* Compute the matrices (D^-1) and (D^-1 dot^2D D^-1) */
  for (sci = 0; sci < 12; ++sci) {
    start_time = start_timer();

    assign_sd2sd(VOLUME, gbuffer[sci], psi);
    Dw_temporal_dble(2, psi, eta);
    solve_dirac(eta, gbuffer[12 + sci], 0, stat);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    stop_timer(&tinfo, start_time);
  }

  /* Trace 2 can be computed with these two sets */
  full_trace_contribution(out[1], gbuffer + 12, gbuffer);

  /* Unsmear the gauge field */
  if (dop.smear) {
    unsmear_fields();
  }

  for (l = 0; l < 2; l++) {
    status[l] = (status[l] + (48 / 2)) / 48;
  }

  release_wsd();
  afree(bndry_buffer);

  return tinfo;
}
