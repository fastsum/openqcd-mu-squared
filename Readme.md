# openQCD Mu-Squared Computation Program

openqcd-mu-squared provides functionality for computing the terms necessary to
evaluating the second order in chemical potential Taylor expansion contribution
to mesonic correlation functions. It is built on top of openqcd-fastsum, and
therefore make use of all the performance considerations that exist withing that
code base.

There are two main contributions to the correlation functions, the one-all
terms, namely terms that are of a correlation function-like form, and the
all-all terms, which represent bubble diagrams. More information about its
mathematical form can be found in the documentation.

## Building

To compile the project simply `cd` into the `build` directory and run `make`.
See the Readme file inside of the `build` directory for more details.

### Requirements

 * v1.1 of [openqcd-fastsum][openqcd-fastsum lib]
 * v1.0 of [openqcd-propagator][openqcd-propagator lib]
 * an implementation of MPI

## Usage

The main output of the program is the `openqcd-mu-squared` executable which runs
through pre-existing lattice gauge configurations and computes its contribution
to the mu-squared terms. The program has the following usage:

```
usage: openqcd-mu-squared -i <input file> [-a]

Program Parameters:
-i <input file>  Name of the input file from which the simulation parameters are
                 read

Optional parameters:
-a               Run in append mode to continue a previously started run from
                 the last successful computation
```

The program infile is has many of the same sections as openqcd, and an example
of a complete infile can be found at `main/examples/mu-squared.in`. The only new
section as compared to those of `openqcd-propagator` is a short section on the
number of noise vectors to use for the estimate:

```
[Noise vectors]
num 100
```

It also supports the following definition of the deflation subspace parameters:

```
[Deflation subspace]
bs      4 4 4 4
Ns      28
retries 100
```

where `retries` tells the program how many times to attempt to make a deflation
subspace before it gives up. This is necessary due to a [bug][deflation bug] in
openqcd-fastsum, and should not be necessary once this has been fixed.


## Authors

This code was written by Jonas Rylund Glesaaen while a post-doc in the lattice
group at Swansea University.

## License

The software may be used under the terms of the GNU General Public Licence
(GPL).

[openqcd-fastsum lib]: https://gitlab.com/fastsum/openqcd-fastsum
[openqcd-propagator lib]: https://gitlab.com/fastsum/openqcd-propagator
[deflation bug]: https://gitlab.com/fastsum/openqcd-fastsum/issues/81
